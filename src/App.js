import React from 'react';
import './App.css';
import Login from './components/Login';
import TranslationPage from './components/TranslationPage'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import NotFound from './components/NotFound';
import UserPage from './components/UserPage';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/translate" component={ TranslationPage } />
          <Route path="/user" component={ UserPage } />
          <Route exact path="/" component={Login}/>
          <Route path="*" component={ NotFound } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
