import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons';
import { getUserName, setUserName } from './localstorageHandler'
import AppHeader from './AppHeader';
import '../styles/login.css'
import Logo from "../resources/Logo.png"
import { InputGroup, FormControl, Button } from "react-bootstrap";

class Login extends React.Component {
    //initialize state
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            loggedIn: false
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmitName = this.handleSubmitName.bind(this);
    }

    componentDidMount() {
        //Catch the name from localstorage and set state
        let userName = getUserName()
        this.setState({userName, loggedIn: userName === '' ? false : true})
    }

    //update state name
    handleNameChange(event) {
        this.setState({userName: event.target.value})
    }

    //Save the name to localstorage
    handleSubmitName(event) {
        alert('A name was submitted: ' + this.state.userName);
        this.setState({loggedIn:true});
        setUserName(this.state.userName);
        this.props.history.push({pathname: '/translate', state: {userName: this.state.userName}});
    }

    render () {
        const inputstyle = {
            borderRadius: '15px',
            background: 'transparent',
            border: 'none'
        }
        return (
            <div>
                <AppHeader userName={this.state.loggedIn === true ? this.state.userName : ''} />
                <div className="body" >
                    <div className="welcome">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="welcome-message">
                                        <div className="welcome-message-img">
                                            <img src={Logo} alt="logo" id="welcome-message-logo"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col welcome-message-text">
                                    <div className="row">
                                        <h1>Lost in Translation</h1>
                                    </div>
                                    <div className="row">
                                        <h3>Get Started</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="login" >
                        <div id="login-body">
                            <div className="container login-input">
                                <InputGroup className="input-text">
                                    <FormControl style={inputstyle} value={this.state.userName} placeholder="What's your name?" rows="1"  onChange={this.handleNameChange} />
                                    <InputGroup.Append>
                                    <Button style={inputstyle} type="button" onClick={this.handleSubmitName} ><FontAwesomeIcon icon={faArrowAltCircleRight} color="#845EC2" size="2x"/></Button>
                                    </InputGroup.Append>
                                </InputGroup>  
                            </div>
                        </div>
                        <div id="login-footer">          
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;