import React from 'react';
import { Redirect } from "react-router-dom";
import { getUserName } from "./localstorageHandler";

export const WithAuth = Component => props => {
    //Get username from localstorage
    const auth = getUserName();

    //check if the user has registred, and if not redirect to loginpage
    if (auth !== '') {
        return <Component {...props} />
    } else {
        return <Redirect to="/" />
    }
}
 