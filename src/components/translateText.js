import React from "react";

//Translate the text to a list of images
const TranslateText = ({translateText}) => {
    const regex = '[a-zA-Z]';
    let images = translateText.toLowerCase().split('').filter((letter) => letter.match(regex)).map((letter, index) => {
        return (
            <img alt="handsigns" key={index} src={require(`../resources/individual_signs/${letter}.png`)} className="img-responsive" />
        )
    });
    
    return (
        <div>
            {images}
        </div>
    )
}

export default TranslateText;