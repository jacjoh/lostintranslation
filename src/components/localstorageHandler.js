//collect the previous translations from localstorage
export function getPreviousTranslations() {
    let translations = JSON.parse(localStorage.getItem('translations'));
    return translations === null ? [] : translations; 
}

//add a new translation to the list in localstorage
export function addTranslationToList(list, translation) {
    if(list.length === 10){
        list.pop();
        list.unshift(translation);
    } else {
        list.unshift(translation);
    }
    localStorage.setItem('translations', JSON.stringify(list));
}

//delete the list with translations
export function deleteTranslations() {
    localStorage.removeItem('translations');
}

//get the stored username
export function getUserName() {
    let userName = localStorage.getItem('userName');
    return userName != null ? userName : '';
}

//Set the username
export function setUserName(userName) {
    localStorage.setItem('userName', userName);
}

//delete the username
export function deleteUser() {
    localStorage.removeItem('userName');
}