import React from 'react';
import AppHeader from './AppHeader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { getPreviousTranslations, deleteTranslations, getUserName, setUserName, deleteUser } from './localstorageHandler';
import TranslateText from './translateText';
import { WithAuth } from './withAuth';
import "../styles/userPage.css"
import { InputGroup, FormControl, Button } from "react-bootstrap";


class UserPage extends React.Component {
    //initialize state
    constructor(props) {
        super(props);
        this.state = {
            userName: '', 
            previousTranslations: []
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleUpdateName = this.handleUpdateName.bind(this);
        this.getTranslations = this.getTranslations.bind(this);
        this.deleteList = this.deleteList.bind(this);
        this.deleteProfile = this.deleteProfile.bind(this);
    }

    //load the username and translations
    componentDidMount() {
        let userName = getUserName()
        this.setState({userName})
        this.getTranslations()
    }

    //load the previous translations from localstorage
    getTranslations() {
        let previousTranslations = getPreviousTranslations()
        this.setState({
            previousTranslations: previousTranslations
        }); 
    }

    //update the username in localstorage
    handleUpdateName() {
        setUserName(this.state.userName);
    }

    //update the state
    handleNameChange(event) {
        this.setState({userName: event.target.value})
    }

    //remove the translations from localstorage
    deleteList() {
        deleteTranslations();
    }
    
    //remove the user and translations from localstorage
    deleteProfile() {
        deleteUser();
        deleteTranslations();
        this.props.history.push("/");
    }

    render () {
        let translations = this.state.previousTranslations.map(translation => {
            return (

                <div id="user-page-translation" >
                    <div id="user-page-translation-body">
                        <TranslateText translateText={translation} />
                    </div>
                    <div id="user-page-translation-footer">
                        <div id="user-page-footer-text">
                            {translation}   
                        </div>
                    </div>
                </div>
            )
        })
        const inputstyle = {
            borderRadius: '15px',
            background: 'transparent',
            border: 'none'
        }
        return (
            <div>
                <AppHeader userName={this.state.userName}/>
                <div className="name">
                    <h4 style={{marginTop: "20px"}}>Edit Name</h4>
                    
                        <div className="container edit-user-name-input">
                            <InputGroup className="input-text">
                            <FormControl style={inputstyle} value={this.state.userName} placeholder="Edit Name" rows="1"  onChange={this.handleNameChange} />
                                <InputGroup.Append>
                                <Button style={inputstyle} type="button" onClick={this.handleUpdateName} ><FontAwesomeIcon icon={faUserEdit} color="#845EC2" size="2x"/></Button>
                                </InputGroup.Append>
                            </InputGroup>  
                        </div>
                </div>

                <form>
                    <Button id="delete-list" onClick={this.deleteList} type="submit" >Delete List <FontAwesomeIcon icon={faTrashAlt}/></Button>
                    <Button id="delete-profile" onClick={this.deleteProfile} type="button" >Delete User <FontAwesomeIcon icon={faTrashAlt}/></Button>
                </form>

                <h4 style={{marginTop: "20px"}}>Translation History:</h4>
                <div className="history">
                    {translations}
                </div>
                <div className="actions">
                
                
                
                </div>
            </div>
        )
    }
}

export default WithAuth(UserPage);