import React from 'react';
import AppHeader from './AppHeader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons';
import { getPreviousTranslations, addTranslationToList, getUserName } from './localstorageHandler';
import TranslateText from './translateText';
import { WithAuth } from "./withAuth";
import {Button, InputGroup, FormControl } from "react-bootstrap";
import '../styles/translationPage.css'

class TranslationPage extends React.Component {
    constructor(props) {
        super(props);
        //this.props.location.state.userName
        this.state = {
            userName: '',
            translateText: '',
            handSigns: [],
            previousTranslations: [],
            showTranslation: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmitText = this.handleSubmitText.bind(this);
    }

    //Get the name when mounting, and the previous translations
    componentDidMount() {
        this.setState({userName: getUserName()})
        this.getTranslations()
    }

    //handle change in input
    handleChange(event) {
        //set the show translation text to false if one edits the input text
        if(event.target.value.length < this.state.translateText.length || 
            ((event.target.value > this.state.translateText) && (this.state.showTranslation ===true)))
        {
            this.setState({showTranslation: false})
        }

        //update the translation text
        this.setState({translateText: event.target.value})
    }

    //show the translation and add the translation to the list
    handleSubmitText() {
        this.setState({showTranslation: true});
        this.addTranslation()
    }

    //Get the previous translations from localstorage
    getTranslations() {
        let previousTranslations = getPreviousTranslations()
        this.setState({
            previousTranslations: previousTranslations
        }); 
    }

    //Add the translation to localstorage, and update the list of translations
    addTranslation() {
        addTranslationToList(this.state.previousTranslations, this.state.translateText);
        this.setState({
            previousTranslations: [...this.state.previousTranslations]
        }); 
    }

    render () {
        const inputstyle = {
            borderRadius: '15px',
            background: 'transparent',
            border: 'none'
        }
        return (
            <div>
                <AppHeader userName={this.state.userName}/>
                
                <div className="translation-page-body">
                    <div className="translation-text">
                        <InputGroup className="input-text">
                            <FormControl style={inputstyle} placeholder="Type text to translate" rows="2"  value={this.state.translateText} onChange={this.handleChange} />
                            <InputGroup.Append>
                            <Button style={inputstyle} type="button" onClick={this.handleSubmitText} ><FontAwesomeIcon icon={faArrowAltCircleRight} color="#845EC2" size="2x"/></Button>
                            </InputGroup.Append>
                        </InputGroup>  
                    </div>
                </div>

                <div id="translation" >
                    <div id="translation-body">
                        { this.state.showTranslation ? <TranslateText translateText={this.state.translateText} /> : null }
                    </div>
                    <div id="translation-footer">
                        <div id="footer-text">
                              translation   
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default WithAuth(TranslationPage);