import Logo from '../resources/Logo.png'
import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { useHistory } from "react-router";
import { Navbar, Nav } from "react-bootstrap";
import '../styles/appHeader.css'



function AppHeader (props) {
    const history = useHistory();

    //push the url to user page
    function goToUserPage() {
        history.push({pathname: '/user'})
    }

    //push the url to translation page
    function goToTranslatePage () {
        history.push({pathname: '/translate'})
    }
    return (
        <header className="app-header">
            <Navbar className="justify-content-between" variant="dark">
                <Navbar.Brand className="brand" onClick={goToTranslatePage}>
                    <h3>
                        <img src={Logo} alt="logo" id="logo"/>
                        Lost in Translation
                    </h3>
                </Navbar.Brand>

                <Nav >
                    <Nav.Link  >
                        <button className="user-page-btn" onClick={goToUserPage}>
                            {props.userName}
                            <FontAwesomeIcon className="user-page-logo" icon={faUserCircle} size="3x" color="white"/>
                        </button>
                    </Nav.Link>
                </Nav>
            </Navbar>

        </header>
    )
}




export default AppHeader;