import React from 'react';
import { useLocation } from "react-router-dom";

//Function for 404 page
function NotFound() {
    let location = useLocation();
    return (
        <div>
            <h3 style={{color:'red'}}>
                404: No match for <code>{location.pathname}</code>
            </h3>
        </div>
    );
} 

export default NotFound;